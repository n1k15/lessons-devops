#!/bin/bash
if [ -n "$1" ]; then

    while [ -n "$1" ]; do
        case "$1" in
        -b)
            param="$2"
            df -h $param | sed -n '/sd/p'
            shift
            ;;
        -i)
            IFS=$'\n'
            echo "Выберите диск:"
            for var in $(blkid | awk -F':' '{print $1}'); do
                array=($(blkid | awk -F':' '{print $1}' | sed -n '/sd/p'))
            done
            select disk in "${array[@]}"; do
                echo
                echo "Вы выбрали $disk"
                echo
                df -h $disk
                break
            done
            ;;
        *)
            echo "Такой опции нету."
            echo "Доступные опции:"
            echo "-d <адрес диска> / Выводит информацию только по этому диску"
            echo "-i / дает выбрать диск для которого нужно вывести информацию"
            ;;
        esac
        shift
    done
else
    df -h | sed -n '/sd/p'
fi