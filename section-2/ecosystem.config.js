module.exports = {
  apps: [
    {
      name: 'app',
      script: '/tmp/t6.sh',
      intepreter: '/bin/bash',
      autorestart: false,
      env: {
        LOG_FILE: '/tmp/log',
      },

    },
  ],
};